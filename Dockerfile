FROM ruby:2.6.3
RUN apt-get update -qq && apt-get install -y nodejs npm
RUN npm install -g yarn
RUN mkdir /alitwitter
WORKDIR /alitwitter
COPY Gemfile /alitwitter/Gemfile
RUN gem install bundler:2.1.4
COPY . /alitwitter
RUN bundle install
RUN yarn

# Add a script to be executed every time the container starts.
COPY entrypoint.sh /usr/bin/
RUN chmod +x /usr/bin/entrypoint.sh
ENTRYPOINT ["entrypoint.sh"]

EXPOSE 3000

CMD ["bundle", "exec", "rails", "server", "-b", "0.0.0.0"]
