class TweetsController < ApplicationController
  def new
    @tweets = Tweet.all.reverse
  end

  def create
    @tweet = Tweet.new(params.require(:tweet).permit(:text))
    respond_to do |format|
      if @tweet.valid?
        @tweet.save
        format.html { redirect_to new_tweet_path, notice: 'Tweet was successfully created.' }
        format.json { render :new, status: :created, location: @tweet }
      else
        format.html { redirect_to new_tweet_path, notice: @tweet.errors[:text][0] }
        format.json { render :new, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @tweet = Tweet.find(params[:id])
    respond_to do |format|
      if @tweet.present?
        @tweet.destroy
        format.html { redirect_to new_tweet_path, notice: 'Tweet was successfully deleted.' }
        format.json { head :no_content }
      end
    end
  end
end
